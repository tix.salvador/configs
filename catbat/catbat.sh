#!/bin/bash
THEME=$(cat ~/.config/catbat/catbatthemes | shuf -n 1)

batcat -p  --theme="$THEME" $@

exit

