set encoding=utf-8
set number relativenumber
syntax enable
set noswapfile
set scrolloff=7
set backspace=indent,eol,start
set mouse=

"Python specific
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set fileformat=unix

let mapleader = '\'

call plug#begin()

Plug 'https://github.com/vim-airline/vim-airline' " Status bar
Plug 'https://github.com/preservim/nerdtree' " NerdTree
Plug 'https://github.com/ap/vim-css-color' " CSS Color Preview
Plug 'https://github.com/rafi/awesome-vim-colorschemes' " Retro Scheme
Plug 'https://github.com/ryanoasis/vim-devicons' " Developer Icons
Plug 'https://github.com/tc50cal/vim-terminal' " Vim Terminal
Plug 'https://github.com/terryma/vim-multiple-cursors' " CTRL + N for multiple cursors
Plug 'https://github.com/preservim/tagbar' " Tagbar for code navigation
Plug 'https://github.com/pearofducks/ansible-vim' "Ansible plugin 
Plug 'https://github.com/hashivim/vim-terraform' "Terraform plugin
Plug 'https://github.com/nvim-telescope/telescope.nvim' " Telescope plugin
Plug 'https://github.com/nvim-lua/plenary.nvim' " Telescope plugin dependency
Plug 'https://github.com/voldikss/vim-floaterm' "Floaterm plugin
Plug 'https://github.com/akinsho/toggleterm.nvim', {'tag' : '*'} "Toggleterm plugin
Plug 'https://github.com/vimwiki/vimwiki' "Vimwiki plugin
Plug 'morhetz/gruvbox' "Gruvbox plugin
Plug 'vim-airline/vim-airline-themes'
Plug 'christoomey/vim-tmux-navigator' "tmux navigator
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " Tree-sitter
"Plug 'norcalli/nvim-colorizer.lua'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

call plug#end()

nnoremap <C-\> :ToggleTerm size=15<CR>

" fzf find 
" nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>ff <cmd>:Ag<cr>
nnoremap <leader>dd <cmd>:Files<cr>

"nmap <F5> :FloatermNew --position=topright --width=0.75 --height=0.85 lazygit<CR>
nmap <F5> :FloatermNew --position=topright --width=0.75 --height=0.85<CR>
let g:floaterm_keymap_toggle = '<F6>'
nmap <F8> :TagbarToggle<CR>
nmap <F9> :Telescope colorscheme<CR>

" Nerdtree
nnoremap <C-t> :NERDTreeToggle<CR>
"let g:NERDTreeDirArrowExpandable="+"
"let g:NERDTreeDirArrowCollapsible="~"
let NERDTreeQuitOnOpen=1

colorscheme gruvbox
let g:airline_theme='gruvbox'

lua require("toggleterm").setup()

" Nerdcommenter
nmap <C-_> <Plug>NERDCommenterToggle
vmap <C-_> <Plug>NERDCommenterTogggle<CR>gv

" Tabs
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#fnamemode=':t'
nmap <leader>1 :bp<CR>
nmap <leader>2 :bn<CR>

" vimwiki prevent url shortening  
let g:vimwiki_url_maxsave=0

lua require'nvim-treesitter.configs'.setup {ignore_install = { "javascript" }, highlight = {enable = true,disable = { "vim" },additional_vim_regex_highlighting = false,},}

" Start NERDTree and leave the cursor in it.
" autocmd VimEnter * NERDTree
