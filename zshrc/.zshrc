ohmyposh="/home/edge/.zshrc.start_ohmyposh"
powerline="/home/edge/.zshrc.start_powerline"
NUM=`seq 1 10 | shuf -n 1`

if [ -f "$ohmyposh" ] && [ -f "$powerline" ]; then
        #source $(find ~ -type f -name ".zshrc.start*" | shuf -n1)
        #source $(find ~ -path ~/.local  -prune -o  -type f -name ".zshrc.start*" | grep -v local  | shuf -n1)
        if [ $NUM -eq 1 ]; then
            source /home/edge/.zshrc.start_powerline
        else
            source /home/edge/.zshrc.start_ohmyposh
       fi
else 
    echo "no .zsh config found"
fi

# History
HISTSIZE=2000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

